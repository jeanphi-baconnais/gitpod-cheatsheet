# 🎨 Gitpod-Cheatsheet

✨ I created 2 Cheatsheet about Gitpod. You can find them on my [Dev.to profile](https://dev.to/jphi_baconnais) : 

- Gitpod : https://dev.to/gitpod/gitpod-cheatsheet-2ek6

- Gitpod CLI : https://dev.to/gitpod/gitpod-cli-cheatsheet-55hj


By Jean-Phi